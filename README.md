# Spotify-cmd

Little cli client for spotify, with a terminal gui integrated

## Install instructions

```bash

go get -u gitlab.com/sywesk/spotify-cmd
cd $GOROOT/src/gitlab.com/sywesk/spotify-cmd/src
dep ensure
go build 

```

The build process will be improved in future versions.
