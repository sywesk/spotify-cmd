package main

import (
	"os"

	"github.com/pkg/errors"
)

func handleCommand(cmd string, state *UIState) error {
	var err error = nil

	if cmd == "play" {
		err = state.controller.Play(nil)
	} else if cmd == "pause" {
		err = state.controller.Pause()
	} else if cmd == "playlists" {
		state.app.SetFocus(state.GetComponent("playlists"))
	} else if cmd == "exit" {
		state.app.Stop()
		os.Exit(0)
	} else {
		err = errors.Errorf("unknown command %s", cmd)
	}

	return err
}
