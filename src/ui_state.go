package main

import (
	"fmt"
	"time"

	"github.com/rivo/tview"
)

type UICallback func(state *UIState)

type UIStateData struct {
	playback               *SpotifyCurrentPlayback
	deviceList             *SpotifyDeviceList
	playlists              []*SpotifySimplifiedPlaylist
	selectedPlaylist       *SpotifySimplifiedPlaylist
	selectedPlaylistTracks []*SpotifyPlaylistTrack
}

type UIDebug struct {
	textView  *tview.TextView
	debugData string
	enabled   bool
}

type UIState struct {
	controller *SpotifyController
	app        *tview.Application
	debug      *UIDebug
	components map[string]tview.Primitive

	data                 UIStateData
	dataFetchCallbacks   []UICallback
	dataRefreshCallbacks []UICallback
	dataUpdateInterval   time.Duration
	handlerStarted       bool
}

func NewUIState(controller *SpotifyController, app *tview.Application) *UIState {
	return &UIState{
		controller: controller,
		app:        app,
		debug: &UIDebug{
			textView:  tview.NewTextView(),
			debugData: "",
			enabled:   true,
		},
		components: map[string]tview.Primitive{},

		data:                 UIStateData{},
		dataFetchCallbacks:   []UICallback{},
		dataRefreshCallbacks: []UICallback{},
		dataUpdateInterval:   1 * time.Second,
		handlerStarted:       false,
	}
}

func (self *UIState) AddFetchCallback(callback UICallback) {
	self.dataFetchCallbacks = append(self.dataFetchCallbacks, callback)
}

func (self *UIState) AddRefreshCallback(callback UICallback) {
	self.dataRefreshCallbacks = append(self.dataRefreshCallbacks, callback)
}

func (self *UIState) Start() {
	if self.handlerStarted {
		return
	}

	self.handlerStarted = true
	go self.dataUpdateLoop()
}

func (self *UIState) dataUpdateLoop() {
	for {
		for _, callback := range self.dataFetchCallbacks {
			callback(self)
		}

		for _, callback := range self.dataRefreshCallbacks {
			callback(self)
		}

		self.app.Draw()
		time.Sleep(self.dataUpdateInterval)
	}
}

func (self *UIState) Debug(msg string) {
	if !self.debug.enabled {
		return
	}

	self.debug.debugData += msg + "\n"
	self.debug.textView.SetText(self.debug.debugData)
	self.debug.textView.ScrollToEnd()
	self.app.Draw()
}

func (self *UIState) DebugObject(obj interface{}) {
	self.Debug(fmt.Sprintf("%#v", obj))
}

func (self *UIState) AddComponent(name string, primitive tview.Primitive) {
	self.components[name] = primitive
}

func (self *UIState) GetComponent(name string) tview.Primitive {
	return self.components[name]
}
