package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	MaxPlaylistPerPage = 50
	MaxTracksPerPage   = 100
)

type SpotifyController struct {
	credentials *SpotifyCredentials
	httpClient  *http.Client
}

func (self *SpotifyController) Play(request *SpotifyPlayRequest) error {
	body := ""
	if request != nil {
		jsonBody, err := json.Marshal(request)
		if err != nil {
			return err
		}

		body = string(jsonBody)
	}

	resp, err := self.callApi("PUT", "https://api.spotify.com/v1/me/player/play", body, nil)
	if err != nil {
		return err
	} else if resp.StatusCode == 403 {
		return errors.New("spotify api error: the user is not premium")
	} else if resp.StatusCode == 404 {
		return errors.New("spotify api error: no device found")
	}

	return nil
}

func (self *SpotifyController) Pause() error {
	resp, err := self.callApi("PUT", "https://api.spotify.com/v1/me/player/pause", "", nil)
	if err != nil {
		return err
	} else if resp.StatusCode == 403 {
		return errors.New("spotify api error: the user is not premium")
	} else if resp.StatusCode == 404 {
		return errors.New("spotify api error: no device found")
	}

	return nil
}

func (self *SpotifyController) GetCurrentPlayback() (*SpotifyCurrentPlayback, error) {
	playback := &SpotifyCurrentPlayback{}
	_, err := self.callApi("GET", "https://api.spotify.com/v1/me/player", "", playback)

	return playback, err
}

func (self *SpotifyController) GetDevices() (*SpotifyDeviceList, error) {
	deviceList := &SpotifyDeviceList{}
	_, err := self.callApi("GET", "https://api.spotify.com/v1/me/player/devices", "", deviceList)

	return deviceList, err
}

func (self *SpotifyController) GetPlaylists() ([]*SpotifySimplifiedPlaylist, error) {
	result := make([]*SpotifySimplifiedPlaylist, 0)

	for {
		playlists := &SpotifyPlaylistList{}

		args := &url.Values{}
		args.Set("offset", strconv.Itoa(len(result)))
		args.Set("limit", strconv.Itoa(MaxPlaylistPerPage))

		resp, err := self.callApi("GET", "https://api.spotify.com/v1/me/playlists", args.Encode(), playlists)
		if err != nil {
			return nil, err
		} else if resp.StatusCode == 400 {
			return nil, errors.New("spotify api error: the limit must be changed, caused a bad request")
		}

		for _, playlist := range playlists.Items {
			result = append(result, playlist)
		}

		if len(result) >= playlists.Total {
			break
		}
	}

	return result, nil
}

func (self *SpotifyController) GetPlaylistTracks(playlistHRef string) ([]*SpotifyPlaylistTrack, error) {
	result := make([]*SpotifyPlaylistTrack, 0)

	for {
		tracks := &SpotifyPlaylistTracksPage{}

		args := &url.Values{}
		args.Set("offset", strconv.Itoa(len(result)))
		args.Set("limit", strconv.Itoa(MaxTracksPerPage))

		resp, err := self.callApi("GET", playlistHRef+"/tracks", args.Encode(), tracks)
		if err != nil {
			return nil, err
		} else if resp.StatusCode == 400 {
			return nil, errors.New("spotify api error: the limit must be changed, caused a bad request")
		}

		for _, track := range tracks.Items {
			result = append(result, track)
		}

		if len(result) >= tracks.Total {
			break
		}
	}

	return result, nil
}

func (self *SpotifyController) callApi(method string, url string, args string, v interface{}) (*http.Response, error) {
	for {
		resp, err := self.doCallApi(method, url, args, v)
		if err != nil {
			return nil, err
		} else if resp.StatusCode != 429 {
			return resp, nil // this function will return here most of the time
		}

		retry_after := resp.Header.Get("Retry-After")
		if retry_after == "" {
			return nil, errors.New("spotify api error: got 429 too many requests, but no Retry-After header")
		}

		delay, err := strconv.Atoi(retry_after)
		if err != nil {
			return nil, errors.Wrapf(err, "spotify api error: couldn't convert the retry after header value '%s' to an int", retry_after)
		}

		log.Infof("waiting %d seconds before calling the api again", delay)
		time.Sleep(time.Duration(delay+1) * time.Second)
	}
}

func (self *SpotifyController) doCallApi(method string, url string, args string, v interface{}) (*http.Response, error) {
	self.credentials.refreshTokenIfExpired()

	var body io.Reader = nil
	if args != "" {
		if method == "GET" {
			url = url + "?" + args
		} else {
			body = strings.NewReader(args)
		}
	}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+self.credentials.accessToken)

	resp, err := self.httpClient.Do(req)
	if err != nil {
		return nil, err
	} else if resp.StatusCode == 204 {
		return resp, nil
	} else if resp.StatusCode >= 500 {
		return resp, errors.Errorf("spotify api error: internal server error %d", resp.StatusCode)
	}

	respContent, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return resp, err
	} else if len(respContent) == 0 {
		return resp, nil
	}

	if v == nil {
		return resp, nil
	}

	err = json.Unmarshal(respContent, v)
	return resp, err
}

func NewSpotifyContronller(credentials *SpotifyCredentials) *SpotifyController {
	if credentials == nil {
		log.Fatal("credentials cannot be null")
	}

	return &SpotifyController{
		credentials: credentials,
		httpClient:  &http.Client{},
	}
}
