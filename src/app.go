package main

import (
	"fmt"
)

var version = "0.0.1"

func main() {
	fmt.Printf("spotify-cmd %s\n", version)

	credentials := GetCredentials()
	controller := NewSpotifyContronller(credentials)

	startUI(controller)
}
