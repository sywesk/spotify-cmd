package main

type SpotifyDevice struct {
	ID               string `json:"id"`
	IsActive         bool   `json:"is_active"`
	IsPrivateSession bool   `json:"is_private_session"`
	IsRestricted     bool   `json:"is_restricted"`
	Name             string `json:"name"`
	Type             string `json:"computer"`
	VolumePercent    int    `json:"volume_percent"`
}

type SpotifyExternalURLs struct {
	Spotify string `json:"spotify"`
}

type SpotifyExternalIDs struct {
	ISRC string `json:"isrc"`
	EAN  string `json:"ean"`
	UPC  string `json:"upc"`
}

type SpotifySimplifiedArtist struct {
	ExternalURLs *SpotifyExternalURLs `json:"external_urls"`
	HRef         string               `json:"href"`
	ID           string               `json:"id"`
	Name         string               `json:"name"`
	Type         string               `json:"type"`
	URI          string               `json:"uri"`
}

type SpotifyImage struct {
	Height int    `json:"height"`
	Width  int    `json:"width"`
	URL    string `json:"url"`
}

type SpotifyRestrictions struct {
	Reason string `json:"reason"`
}

type SpotifySimplifiedAlbum struct {
	AlbumGroup           string                     `json:"album_group"`
	AlbumType            string                     `json:"album_type"`
	AvailableMarkets     []string                   `json:"available_markets"`
	Artists              []*SpotifySimplifiedArtist `json:"artists"`
	ExternalURLs         *SpotifyExternalURLs       `json:"external_urls"`
	HRef                 string                     `json:"href"`
	ID                   string                     `json:"id"`
	Images               []*SpotifyImage            `json:"images"`
	Name                 string                     `json:"name"`
	ReleaseDate          string                     `json:"release_date"`
	ReleaseDatePrecision string                     `json:"release_date_precision"`
	Type                 string                     `json:"type"`
	URI                  string                     `json:"uri"`
	Restrictions         *SpotifyRestrictions       `json:"restrictions"`
}

type SpotifyContext struct {
	Type         string               `json:"type"`
	Href         string               `json:"href"`
	ExternalURLs *SpotifyExternalURLs `json:"external_urls"`
	URI          string               `json:"uri"`
}

type SpotifyTrackLink struct {
	ExternalURLs *SpotifyExternalURLs `json:"external_urls"`
	Href         string               `json:"href"`
	ID           string               `json:"id"`
	Type         string               `json:"type"`
	URI          string               `json:"uri"`
}

type SpotifyTrack struct {
	Album            *SpotifySimplifiedAlbum    `json:"album"`
	Artists          []*SpotifySimplifiedArtist `json:"artists"`
	AvailableMarkets []string                   `json:"available_markets"`
	DiscNumber       int                        `json:"disc_number"`
	DurationMS       int                        `json:"duration_ms"`
	Explicit         bool                       `json:"explicit"`
	ExternalIDs      *SpotifyExternalIDs        `json:"external_ids"`
	ExternalURLs     *SpotifyExternalURLs       `json:"external_urls"`
	HRef             string                     `json:"href"`
	ID               string                     `json:"id"`
	IsPlayable       bool                       `json:"is_playable"`
	LinkedFrom       *SpotifyTrackLink          `json:"linked_track"`
	Restrictions     *SpotifyRestrictions       `json:"restrictions"`
	Name             string                     `json:"name"`
	Popularity       int                        `json:"popularity"`
	PreviewURL       string                     `json:"preview_url"`
	TrackNumber      int                        `json:"track_number"`
	Type             string                     `json:"type"`
	URI              string                     `json:"uri"`
}

type SpotifySimplifiedTrack struct {
	Artists          []*SpotifySimplifiedArtist `json:"artists"`
	AvailableMarkets []string                   `json:"available_markets"`
	DiscNumber       int                        `json:"disc_number"`
	DurationMS       int                        `json:"duration_ms"`
	Explicit         bool                       `json:"explicit"`
	ExternalURLs     *SpotifyExternalURLs       `json:"external_urls"`
	HRef             string                     `json:"href"`
	ID               string                     `json:"id"`
	IsPlayable       bool                       `json:"is_playable"`
	LinkedFrom       *SpotifyTrackLink          `json:"linked_track"`
	Restrictions     *SpotifyRestrictions       `json:"restrictions"`
	Name             string                     `json:"name"`
	PreviewURL       string                     `json:"preview_url"`
	TrackNumber      int                        `json:"track_number"`
	Type             string                     `json:"type"`
	URI              string                     `json:"uri"`
}

type SpotifyCurrentPlayback struct {
	Device       *SpotifyDevice  `json:"device"`
	RepeatState  string          `json:"repeat_state"`
	ShuffleState bool            `json:"shuffle_state"`
	Context      *SpotifyContext `json:"context"`
	Timestamp    int64           `json:"timestamp"`
	ProgressMS   int             `json:"progress_ms"`
	IsPlaying    bool            `json:"is_playing"`
	Item         *SpotifyTrack   `json:"item"`
}

type SpotifyDeviceList struct {
	Devices []*SpotifyDevice `json:"devices"`
}

type SpotifyFollowers struct {
	HRef  string `json:"href"`
	Total int    `json:"total"`
}

type SpotifyPublicUser struct {
	DisplayName  string               `json:"display_name"`
	ExternalURLs *SpotifyExternalURLs `json:"external_urls"`
	Followers    *SpotifyFollowers    `json:"followers"`
	HRef         string               `json:"href"`
	ID           string               `json:"id"`
	Images       []*SpotifyImage      `json:"images"`
	Type         string               `json:"type"`
	URI          string               `json:"uri"`
}

type SpotifyPaging struct {
	HRef     string `json:"href"`
	Limit    int    `json:"limit"`
	Next     string `json:"next"`
	Offset   int    `json:"offset"`
	Previous string `json:"previous"`
	Total    int    `json:"total"`
}

type SpotifyPlaylistTrack struct {
	AddedAt string             `json:"added_at"`
	AddedBy *SpotifyPublicUser `json:"added_by"`
	IsLocal bool               `json:"is_local"`
	Track   *SpotifyTrack      `json:"track"`
}

type SpotifyPlaylistTracksPage struct {
	SpotifyPaging
	Items []*SpotifyPlaylistTrack `json:"items"`
}

type SpotifyPlaylist struct {
	Collaborative bool                       `json:"collaborative"`
	Description   string                     `json:"description"`
	ExternalURLs  *SpotifyExternalURLs       `json:"external_urls"`
	Followers     *SpotifyFollowers          `json:"followers"`
	HRef          string                     `json:"href"`
	ID            string                     `json:"id"`
	Images        []*SpotifyImage            `json:"images"`
	Name          string                     `json:"name"`
	Owner         *SpotifyPublicUser         `json:"owner"`
	Public        bool                       `json:"public"`
	SnapshotID    string                     `json:"snapshot_id"`
	Tracks        *SpotifyPlaylistTracksPage `json:"tracks"`
	Type          string                     `json:"type"`
	URI           string                     `json:"uri"`
}

type SpotifyPlaylistTracks struct {
	HRef  string `json:"href"`
	Total int    `json:"total"`
}

type SpotifySimplifiedPlaylist struct {
	Collaborative bool                   `json:"collaborative"`
	ExternalURLs  *SpotifyExternalURLs   `json:"external_urls"`
	HRef          string                 `json:"href"`
	ID            string                 `json:"id"`
	Images        []*SpotifyImage        `json:"images"`
	Name          string                 `json:"name"`
	Owner         *SpotifyPublicUser     `json:"owner"`
	Public        bool                   `json:"public"`
	SnapshotID    string                 `json:"snapshot_id"`
	Tracks        *SpotifyPlaylistTracks `json:"tracks"`
	Type          string                 `json:"type"`
	URI           string                 `json:"uri"`
}

type SpotifyPlaylistList struct {
	SpotifyPaging
	Items []*SpotifySimplifiedPlaylist `json:"items"`
}

// -------------------

type SpotifyPlayRequestOffset struct {
	Position int    `json:"position,omitempty"`
	URI      string `json:"uri,omitempty"`
}

type SpotifyPlayRequest struct {
	ContextURI string                    `json:"context_uri"`
	Offset     *SpotifyPlayRequestOffset `json:"offset,omitempty"`
}
