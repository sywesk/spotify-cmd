package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime"
	"strings"
	"time"
)

func readStdinString(varName string, expectedLen int) string {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("Please type in %s: ", varName)
		value, err := reader.ReadString('\n')
		if err != nil {
			log.Fatalf("an error occured while reading stdin: %s", err.Error())
		}

		value = strings.TrimSuffix(value, "\n")
		value = strings.TrimSuffix(value, "\r")

		if expectedLen != 0 && len(value) != expectedLen {
			fmt.Printf("The length (%d) doesn't match the expected length of %d characters\n", len(value), expectedLen)
			continue
		}

		return value
	}
}

func randString(length int) string {
	rand.Seed(time.Now().UnixNano())

	letters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	result := make([]byte, length)
	for i := 0; i < length; i++ {
		result[i] = letters[rand.Int()%len(letters)]
	}

	return string(result)
}

func UserHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}

func boolToString(b bool) string {
	if b {
		return "true"
	} else {
		return "false"
	}
}

func msToReadableTime(ms int) string {
	remainder := ms

	var hours int = remainder / 3600000
	remainder = remainder - hours*3600000

	var minutes int = remainder / 60000
	remainder = remainder - minutes*60000

	var seconds int = remainder / 1000
	remainder = remainder - seconds*1000

	result := ""

	if hours != 0 {
		result += fmt.Sprintf("%dh ", hours)
	}

	if minutes != 0 || hours != 0 {
		result += fmt.Sprintf("%dm ", minutes)
	}

	if seconds != 0 || hours != 0 || minutes != 0 {
		result += fmt.Sprintf("%ds ", seconds)
	}

	if remainder != 0 || hours != 0 || minutes != 0 || seconds != 0 {
		result += fmt.Sprintf("%dms ", remainder)
	}

	if len(result) > 0 {
		result = result[:len(result)-1]
	}

	return result
}
