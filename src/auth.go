package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

var CredentialsFile = UserHomeDir() + "/.spotify-cmd"

type SpotifyTokenReply struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	Scope        string `json:"scope"`
	ExpiresIn    int64  `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
}

type SpotifyCredentials struct {
	ClientID     string `json:"client_id"`
	SecretKey    string `json:"secret_key"`
	RefreshToken string `json:"refresh_token"`

	accessToken        string
	accessTokenExpires int64
	state              string
}

func (self *SpotifyCredentials) read() {
	self.ClientID = readStdinString("your client id", 32)
	self.SecretKey = readStdinString("your secret key", 32)
	self.save()
}

func (self SpotifyCredentials) save() {
	log.Info("saving credentials...")

	data, err := json.Marshal(self)
	if err != nil {
		log.Fatal("an error occurred while marshalling credentials: %s", err.Error())
	}

	err = ioutil.WriteFile(CredentialsFile, data, 600)
	if err != nil {
		log.Fatal("an error occurred while writing the credential file '%s': %s", CredentialsFile, err.Error())
	}
}

func (self *SpotifyCredentials) signin() {
	tokenSync := make(chan bool)

	handler := http.HandlerFunc(func(writer http.ResponseWriter, r *http.Request) {
		log.Info("spotify called back...")

		query := r.URL.Query()

		receivedState := query.Get("state")
		if receivedState != self.state {
			log.Fatalf("received an invalid state: had %s got %s", self.state, receivedState)
		} else if apiErr := query.Get("error"); apiErr != "" {
			log.Fatalf("the spotify api returned an error: %s", apiErr)
		}

		code := query.Get("code")
		if code == "" {
			log.Fatalf("the spotify api didn't give an auth code")
		}

		self.getTokensFromCode(code)
		tokenSync <- true
	})

	go func() {
		http.ListenAndServe("127.0.0.1:34058", handler)
	}()

	fmt.Printf("Please open your browser to this url to sign in : %s", self.buildSigninURL())

	<-tokenSync
}

func (self *SpotifyCredentials) refreshTokenIfExpired() {
	// Check that the expiration is at least 5 seconds in the future, to have enough time to make an api call
	if self.accessTokenExpires > time.Now().Unix()+5 {
		return
	}

	log.Info("access token expiring soon, requesting a refresh...")
	self.refreshToken()
}

func (self *SpotifyCredentials) getTokensFromCode(code string) {
	log.Info("getting tokens from authorisation code...")

	form := &url.Values{}

	form.Add("grant_type", "authorization_code")
	form.Add("code", code)
	form.Add("redirect_uri", "http://localhost:34058/callback")
	form.Add("client_id", self.ClientID)
	form.Add("client_secret", self.SecretKey)

	self.callTokenEndpoint(form)
}

func (self *SpotifyCredentials) refreshToken() {
	log.Info("refreshing tokens ...")

	form := &url.Values{}

	form.Add("grant_type", "refresh_token")
	form.Add("refresh_token", self.RefreshToken)
	form.Add("client_id", self.ClientID)
	form.Add("client_secret", self.SecretKey)

	self.callTokenEndpoint(form)
}

func (self *SpotifyCredentials) callTokenEndpoint(form *url.Values) {
	req, err := http.NewRequest("POST", "https://accounts.spotify.com/api/token", strings.NewReader(form.Encode()))
	if err != nil {
		log.Fatalf("failed to build the 'get token' request: %s", err.Error())
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	httpClient := &http.Client{}

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatalf("the 'get token' request failed: %s", err.Error())
	} else if resp.StatusCode != 200 {
		log.Fatalf("the 'get token' request failed: status code is %d instead of 200", resp.StatusCode)
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("failed to read the 'get token' response's body: %s", err.Error())
	}

	reply := &SpotifyTokenReply{}
	err = json.Unmarshal(respBody, reply)
	if err != nil {
		log.Fatalf("failed to unmarshal the 'get token' response's body: %s", err.Error())
	} else if reply.TokenType != "Bearer" {
		log.Fatalf("received an invalid token type: %s instead of Bearer", reply.TokenType)
	}

	self.accessToken = reply.AccessToken
	self.accessTokenExpires = time.Now().Unix() + reply.ExpiresIn
	if reply.RefreshToken != "" {
		self.RefreshToken = reply.RefreshToken
	}

	log.Infof("refresh_token=%s", self.RefreshToken)
	log.Infof("access_token=%s", self.accessToken)
	log.Infof("access_token_expires=%d", self.accessTokenExpires)

	self.save()
}

func (self *SpotifyCredentials) buildSigninURL() string {
	self.state = randString(20)

	scopes := []string{
		"playlist-read-private",
		"playlist-read-collaborative",
		"user-library-read",
		"user-read-playback-state",
		"user-read-recently-played",
		"user-top-read",
		"user-follow-read",
		"user-read-private",
		"user-read-currently-playing",
		"user-modify-playback-state",
	}

	authorize_url := &url.URL{
		Scheme: "https",
		Host:   "accounts.spotify.com",
		Path:   "authorize/",
	}

	query := authorize_url.Query()

	query.Set("client_id", self.ClientID)
	query.Set("response_type", "code")
	query.Set("redirect_uri", "http://localhost:34058/callback")
	query.Set("scope", strings.Join(scopes, " "))
	query.Set("state", self.state)

	authorize_url.RawQuery = query.Encode()

	return authorize_url.String()
}

func loadAppCredentials() *SpotifyCredentials {
	log.Info("loading credentials...")

	if _, err := os.Stat(CredentialsFile); os.IsNotExist(err) {
		log.Infof("the credentials file '%s' doesn't exist", CredentialsFile)
		return nil
	}

	content, err := ioutil.ReadFile(CredentialsFile)
	if err != nil {
		log.Fatalf("an error occurred while reading the credential file '%s': %s", CredentialsFile, err.Error())
	}

	credentials := &SpotifyCredentials{}
	err = json.Unmarshal(content, credentials)
	if err != nil {
		log.Fatalf("an error occurred while unmarshalling the credential file '%s': %s", CredentialsFile, err.Error())
	}

	return credentials
}

func GetCredentials() *SpotifyCredentials {
	credentials := loadAppCredentials()
	if credentials == nil {
		credentials = &SpotifyCredentials{}
	}

	if credentials.ClientID == "" || credentials.SecretKey == "" {
		credentials.read()
	}

	if credentials.RefreshToken == "" {
		credentials.signin()
	}

	if credentials.accessToken == "" {
		credentials.refreshToken()
	}

	return credentials
}
