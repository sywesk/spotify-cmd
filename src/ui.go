package main

import (
	"github.com/rivo/tview"
	"strconv"
	"github.com/gdamore/tcell"
	"fmt"
)

func startUI(controller *SpotifyController) {
	app := tview.NewApplication()

	uiState := NewUIState(controller, app)

	uiState.AddFetchCallback(fetchDevices)
	uiState.AddFetchCallback(fetchPlayback)
	uiState.AddFetchCallback(fetchPlaylists)

	playbackPanel := newPlaybackPanel(uiState)
	commandInput := newCommandInput(uiState)
	devicesPanel := newDevicesPanel(uiState)

	pages := tview.NewPages()

	pages.AddPage("playlists", newPlaylistPage(uiState), true, true)

	pages.ShowPage("playlists")

	playbackColumn := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(playbackPanel, 7, 0, false).
		AddItem(devicesPanel, 0, 1, false)

	centerLayout := tview.NewFlex().
		SetDirection(tview.FlexColumn).
		AddItem(pages, 0, 3, false).
		AddItem(playbackColumn, 0, 1, false)

	layout := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(centerLayout, 0, 3, false)

	if uiState.debug.enabled {
		layout.AddItem(uiState.debug.textView, 0, 1, false)
	}

	layout.AddItem(commandInput, 1, 0, true)

	uiState.Start()

	commandInput.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyUp {
			app.SetFocus(pages)
			return nil
		}

		return event
	})

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyESC {
			app.SetFocus(commandInput)
			return nil
		}

		return event
	})

	err := app.SetRoot(layout, true).Run()
	if err != nil {
		panic(err)
	}
}

func fetchPlayback(state *UIState) {
	state.Debug("playback fetch")
	playback, err := state.controller.GetCurrentPlayback()
	if err != nil {
		state.DebugObject(err)
		state.data.playback = nil
	} else {
		state.data.playback = playback
	}
}

func fetchDevices(state *UIState) {
	state.Debug("devices fetch")
	deviceList, err := state.controller.GetDevices()
	if err != nil {
		state.DebugObject(err)
		state.data.deviceList = nil
	} else {
		state.data.deviceList = deviceList
	}
}

func fetchPlaylists(state *UIState) {
	// The list of playlists doesn't change often, hence we do not need to refresh this data
	if state.data.playlists != nil {
		return
	}

	state.Debug("playlist fetch")
	playlists, err := state.controller.GetPlaylists()
	if err != nil {
		state.DebugObject(err)
		return
	}

	state.data.playlists = playlists
}

func newCommandInput(state *UIState) *tview.InputField {
	input := tview.NewInputField()
	state.AddComponent("command", input)

	input.SetDoneFunc(func(key tcell.Key) {
		command := input.GetText()
		input.SetText("")

		err := handleCommand(command, state)
		if err != nil {
			// ??
		}
	})

	input.SetFieldBackgroundColor(tcell.ColorBlack).
		SetLabel("> ")

	return input
}

func newPlaybackPanel(state *UIState) *tview.Table {
	playbackTable := tview.NewTable()
	state.AddComponent("playback", playbackTable)

	playbackTable.SetBorders(false)
	playbackTable.SetBorder(true).SetTitle("Playback")

	state.AddRefreshCallback(func(state *UIState) {
		playbackPanelRefresh(state, playbackTable)
	})

	return playbackTable
}

func playbackPanelRefresh(state *UIState, table *tview.Table) {
	if state.data.playback == nil {
		table.Clear()
		table.SetCellSimple(0,0, "No device found")
		return
	}

	table.SetCellSimple(0, 0, "track")
	table.SetCellSimple(1, 0, "album")
	table.SetCellSimple(2, 0, "duration")
	table.SetCellSimple(3, 0, "progress")
	table.SetCellSimple(4, 0, "volume")

	trackName := ""
	albumName := ""
	duration := ""

	if state.data.playback.Item != nil {
		trackName = state.data.playback.Item.Name
		duration =  msToReadableTime(state.data.playback.Item.DurationMS)

		if state.data.playback.Item.Album != nil {
			albumName = state.data.playback.Item.Album.Name
		}
	}

	table.SetCellSimple(0, 1, trackName)
	table.SetCellSimple(1, 1, albumName)
	table.SetCellSimple(2, 1, duration)

	table.SetCellSimple(3, 1, msToReadableTime(state.data.playback.ProgressMS))

	volume := ""
	if state.data.playback.Device != nil {
		volume = strconv.Itoa(state.data.playback.Device.VolumePercent) + "%"
	}
	table.SetCellSimple(4, 1, volume)
}

func newDevicesPanel(state *UIState) *tview.Table {
	devicesTable := tview.NewTable()
	state.AddComponent("devices", devicesTable)

	devicesTable.SetBorders(false)
	devicesTable.SetBorder(true).SetTitle("Devices")

	state.AddRefreshCallback(func(state *UIState) {
		devicesPanelAutoUpdate(state, devicesTable)
	})

	return devicesTable
}

func devicesPanelAutoUpdate(state *UIState, table *tview.Table) {
	if state.data.deviceList == nil {
		table.Clear()
		table.SetCellSimple(0,0, "No device found")
		return
	}

	for i, device := range state.data.deviceList.Devices {
		status := ""

		if device.IsActive {
			status += "*"
		}
		if device.IsPrivateSession {
			status += "P"
		}
		if device.IsRestricted {
			status += "R"
		}

		table.SetCellSimple(i, 0, status)
		table.SetCellSimple(i, 1, device.Name)
	}
}

func newPlaylistPage(state *UIState) *tview.Flex {
	playlistList := tview.NewTable()
	state.AddComponent("playlists", playlistList)

	playlistTracks := tview.NewTable()
	state.AddComponent("playlistTracks", playlistTracks)

	playlistList.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyRight {
			row, _ := playlistList.GetSelection()
			selectedPlaylist := state.data.playlists[row]
			state.data.selectedPlaylist = selectedPlaylist

			tracks, err := state.controller.GetPlaylistTracks(selectedPlaylist.HRef)
			if err != nil {
				state.DebugObject(err)
				return nil
			}

			state.data.selectedPlaylistTracks = tracks
			playlistTracksRefresh(state, playlistTracks)
			state.app.SetFocus(playlistTracks)

			return nil
		} else if event.Key() == tcell.KeyEnter {
			row, _ := playlistList.GetSelection()
			selectedPlaylist := state.data.playlists[row]
			state.data.selectedPlaylist = selectedPlaylist

			err := state.controller.Play(&SpotifyPlayRequest{
				ContextURI: state.data.selectedPlaylist.URI,
			})
			if err != nil {
				state.DebugObject(err)
			}

			return nil
		}

		return event
	})

	playlistTracks.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyLeft {
			state.app.SetFocus(playlistList)
			return nil
		} else if event.Key() == tcell.KeyEnter {
			row, _ := playlistTracks.GetSelection()
			selectedTrack := state.data.selectedPlaylistTracks[row]

			err := state.controller.Play(&SpotifyPlayRequest{
				ContextURI: state.data.selectedPlaylist.URI,
				Offset: &SpotifyPlayRequestOffset{
					URI: selectedTrack.Track.URI,
				},
			})
			if err != nil {
				state.DebugObject(err)
			}

			return nil
		}

		return event
	})

	playlistList.SetBorders(false).
		SetSelectable(true, false).
		SetBorder(true)

	playlistTracks.SetBorders(false).
		SetSelectable(true, false).
		SetBorder(true)

	pageLayout := tview.NewFlex().
		SetDirection(tview.FlexColumn).
		AddItem(playlistList, 0, 1, true).
		AddItem(playlistTracks, 0, 2, false)

	state.AddRefreshCallback(func(state *UIState) {
		playlistListRefresh(state, playlistList)
	})

	return pageLayout
}

func playlistListRefresh(state *UIState, table *tview.Table) {
	if table.GetRowCount() != 0 {
		return
	} else if state.data.playlists == nil {
		return
	}

	table.SetTitle(fmt.Sprintf("%d Playlists", len(state.data.playlists)))

	for i, playlist := range state.data.playlists {
		owner := ""
		if playlist.Owner != nil {
			owner = playlist.Owner.DisplayName
		}

		trackCount := ""
		if playlist.Tracks != nil {
			trackCount = strconv.Itoa(playlist.Tracks.Total)
		}

		state := ""
		if playlist.Public {
			state = "pub."
		} else {
			state = "priv."
			if playlist.Collaborative {
				state += ", collab."
			}
		}

		table.SetCell(i, 0, tview.NewTableCell("[orange]" + playlist.Name).SetExpansion(1))

		table.SetCellSimple(i, 1, owner)
		table.SetCellSimple(i, 2, "[aqua]" + trackCount)
		table.SetCellSimple(i, 3, state)
	}

	table.ScrollToBeginning()
}

func playlistTracksRefresh(state *UIState, table *tview.Table) {
	table.Clear()
	table.Select(0,0)

	table.SetTitle(fmt.Sprintf("Playlist '%s' - %d tracks", state.data.selectedPlaylist.Name, len(state.data.selectedPlaylistTracks)))

	if state.data.selectedPlaylistTracks == nil {
		table.SetCellSimple(0, 0, "No tracks found")
	}

	for i, track := range state.data.selectedPlaylistTracks {
		if track.Track == nil {
			state.Debug("ignored playlistTrack with nil track")
			continue
		}

		artistName := ""
		if len(track.Track.Artists) > 0 {
			artistName = track.Track.Artists[0].Name
		}

		albumName := ""
		if track.Track.Album != nil {
			albumName = track.Track.Album.Name
		}

		table.SetCell(i, 0, tview.NewTableCell("[orange]" + track.Track.Name).SetExpansion(1).SetMaxWidth(30))
		table.SetCell(i, 1, tview.NewTableCell(artistName).SetMaxWidth(20))
		table.SetCell(i, 2, tview.NewTableCell(albumName).SetMaxWidth(20))
		table.SetCellSimple(i, 3, msToReadableTime(track.Track.DurationMS))
	}
}
